//Connects to MSSQL Connection Pool and returns the Pool 
const db = require('mssql');
const config = require('../config/keys')


const conn = new db.ConnectionPool(config.db).connect()
.then(conn=>{
    console.log("Connected to Database")
    return conn
})
.catch(err=>{console.error(err)})


// Prepares Stored Procedures for execution using an array of inputs
const sp = (conn,inputs=[])=>{
    const request =  new db.Request(conn)
    inputs.forEach(input=>{
        //Accepts input as name, value,type 
        if(input.length===3){
            request.input(input[0],input[2],input[1])
        }
        else{
            request.input(input[0],input[1])
        }
    })

    return request
}

module.exports.sp = sp
module.exports.db = db
module.exports.conn = conn

