const express = require('express');
const bodyParser = require('body-parser');
const keys = require('./config/keys');
const mainRoutes = require('./routes/routes')
const app = express();
const conn = require('./db/index');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Use Routes
app.use(mainRoutes)

module.exports = app;