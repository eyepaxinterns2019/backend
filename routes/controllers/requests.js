const {db,conn,sp} = require('../../db/index');

// Retrieves Requests from the Database
const showRequests = (req,res)=>{
    conn.then( conn=>{
      sp(conn).execute("dbo.SelectAllRequests",(err,result)=>{
        if(!err){
            res.send(result.recordset)
        }
        else{
            console.error(err)
            res.status(500).send()
        }
      })
    }
    )
    .catch(err=>console.log(err))        
}

module.exports.showRequests = showRequests